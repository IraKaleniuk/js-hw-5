"use strict";

function createNewUser() {
    let fName = prompt("Enter your first name:");
    let lName = prompt("Enter your last name:");

    while (!fName || !lName || fName === "null" || lName === "null") {
        fName = prompt("Enter your first name:", fName);
        lName = prompt("Enter your last name:", lName);
    }
    const newUser = {
        firstName: fName,
        lastName: lName,
        getLogin() {
            const login = (this.firstName[0] + this.lastName).toLowerCase();
            return `Your login is: ${login}`;
        },
        setFirstName(newFirstName) {
            if (newFirstName) {
                Object.defineProperty(this, "firstName", {value: newFirstName});
            }
        },
        setLastName(newLastName) {
            if (newLastName) {
                Object.defineProperty(this, "lastName", {value: newLastName});
            }
        }
    }

    Object.defineProperties(newUser, {
        "firstName": {
            writable: false,
        },
        "lastName": {
            writable: false,
        }
    })

    return newUser;
}

const user1 = createNewUser();
console.log(user1);

const user1Log = user1.getLogin();
console.log(user1Log);

user1.setFirstName(prompt("Enter new first name:"));
console.log(user1);


user1.setLastName(prompt("Enter new last name:"));
console.log(user1);



